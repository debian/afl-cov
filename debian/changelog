afl-cov (0.6.2-1) unstable; urgency=medium

  * New upstream release.
  * deb/control: bump standards to 4.3.0 (no changes needed).
  * deb/copyright: bump copyright span.
  * build with debhelper 11 (changes in d/control and d/compat).

 -- Daniel Stender <stender@debian.org>  Sat, 29 Dec 2018 12:49:34 +0100

afl-cov (0.6.1-2) unstable; urgency=medium

  * deb/control:
    + bump standards to 4.1.3 (no changes needed).
    + update Vcs- fields (project moved to Salsa).
  * deb/copyright:
    + expand copyright span.
  * deb/gbp.conf:
    + drop import-orig section (not needed).
    + change debian-branch to debian/master to meet DEP-14.
    + add sign-tags field, set to True.

 -- Daniel Stender <stender@debian.org>  Mon, 19 Feb 2018 20:50:36 +0100

afl-cov (0.6.1-1) unstable; urgency=medium

  * New upstream release.
  * deb/control: bump standards version to 4.1.1 (no changes needed).

 -- Daniel Stender <stender@debian.org>  Tue, 28 Nov 2017 23:45:32 +0100

afl-cov (0.6-3) unstable; urgency=medium

  * bump debhelper to level 10 (deb/compat and build-dep).
  * dep/copyright:
    + use https in Format field (as preferred by new standards).
    + expand copyright span.
  * bump standards version to 4.0.0 (no further changes needed).

 -- Daniel Stender <stender@debian.org>  Tue, 11 Jul 2017 08:49:56 +0200

afl-cov (0.6-2) unstable; urgency=medium

  * deb/control: updated Vcs fields (source moved to collab-maint).
  * remove deb/.git-dpm, add deb/gbp.conf.

 -- Daniel Stender <stender@debian.org>  Sun, 25 Sep 2016 21:39:57 +0200

afl-cov (0.6-1) unstable; urgency=medium

  * New upstream release.
  * deb/.git-dpm:
    + set patched and upstream tag to NONE.
  * deb/control:
    + bumped standards to 3.9.8 (no changes needed).
    + added .git extension to Vcs-Git.

 -- Daniel Stender <stender@debian.org>  Thu, 09 Jun 2016 08:37:30 +0200

afl-cov (0.5-1) unstable; urgency=medium

  * New upstream release:
    + dropped patches (applied upstream).
  * Package now under git-dpm patch regime (added deb/.git-dpm).
  * deb/control:
    + updated maintainer email address.
    + bumped standards to 3.9.7 (no changes needed).
    + added Vcs-Git and Vcs-Browser.
  * deb/copyright: updated.

 -- Daniel Stender <stender@debian.org>  Tue, 29 Mar 2016 11:19:15 +0200

afl-cov (0.3-1) unstable; urgency=medium

  * Initial release (Closes: #796821).

 -- Daniel Stender <debian@danielstender.com>  Sun, 01 Nov 2015 17:51:16 +0100
